export const LOW = {background: 'linear-gradient(135deg, #a7ff88 81%, #a7ff88 82%, #a7ff88 82%, #ecffc6 100%)'};
export const HIGH = {background: 'linear-gradient(135deg, #ff8888 81%, #ff8888 82%, #ff8888 82%, #ffc6c6 100%)'};

export const ADD_FORM_LOW = {background: '#a7ff88 81%'};
export const ADD_FORM_HIGH = {background: '#ff8888 81%'};
