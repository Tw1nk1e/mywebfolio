import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

import Modal from './Modal';

import { onAddNoteButton } from '../state/actions/Action';

class AddForm extends React.Component {
	static propTypes = {
		onAddNoteButton: PropTypes.func.isRequired,
	}

	state = {
		noteInput: '',
		priority: 'medium',
		showModal: false
	};

	handleShow = () => {
		this.setState({ showModal: true });
	}

	handleHide = () => {
		this.setState({
			showModal: false,
			noteInput: '',
			priority: 'medium'
		});
	}

	handleChangeTextArea = (event) => {
		this.setState({noteInput: event.target.value});
	}

	handleChangePriority = (event) => {
		this.setState({priority: event.target.value});
	}

	handleSubmit = () => {
		this.props.onAddNoteButton(this.state.noteInput, this.state.priority);
		this.setState({
			noteInput: '',
			priority: 'medium',
			showModal: false
		});
	}
	
	render() {

		return (
			<div className="container">
				<div className="row justify-content-center mt-4">
					<button type="button" className="btn btn-primary" onClick={this.handleShow}>
						ADD NOTE
					</button>
					{this.state.showModal &&
						ReactDOM.createPortal(
							<Modal
								handleSubmit={this.handleSubmit}
								handleChangeTextArea={this.handleChangeTextArea}
								noteInput={this.state.noteInput}
								priority={this.state.priority}
								handleChangePriority={this.handleChangePriority}
								handleHide={this.handleHide}
							/>,
							document.getElementById('portal')
						)
					}
				</div>
			</div>
			);
		}
	}

	const mapStateToProps = state => ({
		noteItems: state.noteApp
	});
		
	const mapDispatchToProps = dispatch => (
		bindActionCreators({ onAddNoteButton }, dispatch)
	);

	export default connect(
		mapStateToProps,
		mapDispatchToProps
	)(AddForm);
		