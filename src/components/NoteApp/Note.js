import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EditForm from './EditForm';
import { LOW, HIGH } from '../../styles/components/NoteApp/colorConst';
 
export default class Note extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        priority: PropTypes.string.isRequired,
        noteDelButton: PropTypes.func.isRequired,
        noteEditButton: PropTypes.func.isRequired,
        id: PropTypes.number.isRequired,
        addTime: PropTypes.string.isRequired,
    };

    state = {
        title: '',
        delButtonPush: false,
        editButtonPush: false
    };

    componentDidMount() {
        this.setState({
            title: this.props.title
        });
    }

    onButtonDeletePush = () => {
        this.setState({
            delButtonPush: !this.state.delButtonPush
        });
    }

    onButtonEditPush = () => {
        this.setState({
            editButtonPush: !this.state.editButtonPush
        });
    }

    noteUpdate = (value) => {
        this.setState({ 
            title: value
        });
    }

    render() {

        const {
            id,
            addTime,
            priority,
            noteDelButton,
            noteEditButton
        } = this.props;

        const confirmForm = () => {
            return (
                <div className="confirm-form">
                    <input type="button"
                           className="btn btn-success btn-sm mr-1"
                           onClick={() => noteDelButton(id)}
                           value="YES"
                    />
                    <input type="button"
                           className="btn btn-danger btn-sm"
                           onClick={this.onButtonDeletePush}
                           value="No"
                    />
                </div>
            );
        };

        let element;

        if (this.state.editButtonPush) {
            element = (
                <EditForm 
                    title={this.state.title}
                    noteEditButton={noteEditButton}
                    id={id}
                    onButtonEditPush={this.onButtonEditPush}
                    noteUpdate={this.noteUpdate}
                />
            );
        } else {
            element = (
                <textarea
                className="edit-form"
                type="text"
                rows="6"
                readOnly="readonly"
                value={this.state.title}
                />
            );
        }
        
        const priorityColor = (priority) => {
            if (priority==='low') {
                return LOW;
            } else if (priority==='high') {
                return HIGH;
            } else {
                return null;
            }
        };
        
        return (
            <div className="note" style={priorityColor(priority)}>
                <div>
                    {element}
                </div>
                <div className="note-footer">
                    {!this.state.delButtonPush ?
                        <i className="delButton fas fa-times fa-3x"onClick={this.onButtonDeletePush}/> :
                            confirmForm()
                    }
                    {!this.state.delButtonPush &&
                        <i className="editButton fas fa-pencil-alt fa-2x"
                        onClick={this.onButtonEditPush}/>}
                    <p>
                        {'Add time: ' + addTime}
                    </p>
                </div>
            </div>
        );
    }
}
