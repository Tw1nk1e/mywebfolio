import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class EditForm extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    noteEditButton: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    onButtonEditPush: PropTypes.func.isRequired,
    noteUpdate: PropTypes.func.isRequired
}

  state = {
    title: this.props.title
  }

  handleChange = e => {
    this.setState ({
      title: e.target.value
    });
  }

  onSave = (id, title) => () => {
      this.props.noteEditButton(id, title);
      this.props.onButtonEditPush();
      this.props.noteUpdate(title);
  }

  render() {
    
    return (
      <textarea
          className="edit-form"
          type="text"
          autoFocus="true"
          value={this.state.title}
          onBlur={this.onSave(this.props.id, this.state.title)}
          onChange={this.handleChange}
          maxLength="160"
      />
    );
  }
}
