import React from 'react';
import PropTypes from 'prop-types';
import uid from 'uid';

import Note from './Note';

const NoteList = ({ filteredNotes, actions }) => (
    <div className="container">
        <div className="row justify-content-around justify-content-sm-center">
        {filteredNotes.map(note => {   
            return (
                    <Note title={note.title}
                        id={note.id}
                        priority={note.priority}
                        key={uid()}
                        addTime={note.time}
                        noteDelButton={actions.onDeleteNoteButton}
                        noteEditButton={actions.EditButton}
                        >
                    </Note>);
            })
        }
        </div>
    </div>
);


NoteList.propTypes = {
    filteredNotes: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
};

export default NoteList;