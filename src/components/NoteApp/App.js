import React from 'react';
import VisibleNoteList from './NotelistContainer';
import AddForm from './AddForm';
import Filter from './Filter';

const App = () => (
  <React.Fragment>
    <AddForm />
    <Filter/>
    <VisibleNoteList />
  </React.Fragment>
);

export default App;
