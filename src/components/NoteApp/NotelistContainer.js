import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as NoteAppActions from '../state/actions/Action';
import { getVisibleNotes } from '../selectors/visibleSelector';

import NoteList from './NoteList';

const mapStateToProps = state => ({
  filteredNotes: getVisibleNotes(state)
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(NoteAppActions, dispatch)
});

const VisibleNoteList = connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteList);

export default VisibleNoteList;
