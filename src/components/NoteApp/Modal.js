import React from 'react';
import PropTypes from 'prop-types';

import { ADD_FORM_LOW, ADD_FORM_HIGH } from '../../styles/components/NoteApp/colorConst';

class Modal extends React.Component {
	static propTypes = {
		handleSubmit: PropTypes.func.isRequired,
		handleChangePriority: PropTypes.func.isRequired,
		handleChangeTextArea: PropTypes.func.isRequired,
		noteInput: PropTypes.string.isRequired,
		priority: PropTypes.string.isRequired,
		handleHide: PropTypes.func.isRequired
	}
	
    render() {
		
		const {
			noteInput,
			priority,
			handleSubmit,
			handleHide,
			handleChangePriority,
			handleChangeTextArea
		} = this.props;
		
		const priorityColor = (priority) => {
			if (priority==='low') {
				return ADD_FORM_LOW;
			} else if (priority==='high') {
				return ADD_FORM_HIGH;
			} else {
				return null;
			}
		};

		return (
			<div onClick={handleHide} className="modal fadeIn animated">
				<div
					onClick={e => e.stopPropagation()} //Прекращает дальнейшую передачу текущего события
					className="modal-content-style"
					style={priorityColor(priority)}
				>
					<form onSubmit={handleSubmit}>
							<textarea
								autoFocus
								placeholder="Enter your note here..."
								rows="5"
								value={noteInput}
								onChange={handleChangeTextArea}
								maxLength="150"
								style={priorityColor(priority)}
							/>
					</form>
					<div className="modal-footer-style">
					<select
						className="form-control select-priority_form"
						value={priority} 
						onChange={handleChangePriority}>
							<option value="medium">Norm</option>
							<option value="low">Low</option>
							<option value="high">High</option>
					</select>
					<input
						disabled={!noteInput}
						type="button"
						className="btn btn-primary"
						data-dismiss="modal"
						onClick={handleSubmit}
						value="Save changes"
					/>
					</div>
				</div>
			</div>
		);
	}
}
  
export default Modal;