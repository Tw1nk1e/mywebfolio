import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { setVisibilityFilter } from '../state/actions/Action';

class Filter extends React.Component {
	static propTypes = {
        setVisibilityFilter: PropTypes.func.isRequired,
        filter: PropTypes.string.isRequired
    }
    
    componentDidMount() {
        this.props.setVisibilityFilter('all');
    }

    filterChange = (event) => {
        this.props.setVisibilityFilter(event.target.value);
    }

    render() {
        return (
            <div className="container">
                <div className="custom-radios row justify-content-center py-4">
                    <div>
                        <input
                            type="radio"
                            id="color-1"
                            value="all"
                            checked={this.props.filter === 'all'}
                            onChange={this.filterChange}
                        />
                        <label htmlFor="color-1">
                            <span>
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                            </span>
                        </label>
                    </div>
                    <div>
                        <input
                            type="radio"
                            id="color-2"
                            value="high"
                            checked={this.props.filter === 'high'}
                            onChange={this.filterChange}
                        />
                        <label htmlFor="color-2">
                            <span>
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                            </span>
                        </label>
                    </div>
                    <div>
                        <input
                            type="radio"
                            id="color-3"
                            value="medium"
                            checked={this.props.filter === 'medium'}
                            onChange={this.filterChange}
                        />
                        <label htmlFor="color-3">
                            <span>
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                            </span>
                        </label>
                    </div>
                    <div>
                        <input
                            type="radio"
                            id="color-4"
                            value="low"
                            checked={this.props.filter === 'low'}
                            onChange={this.filterChange}
                        />
                        <label htmlFor="color-4">
                            <span>
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg" alt="Checked Icon" />
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        );
        
    }
}

const mapStateToProps = state => ({
    filter: state.visibilityFilter
});
    
const mapDispatchToProps = dispatch => (
    bindActionCreators({ setVisibilityFilter }, dispatch)
);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Filter);
    