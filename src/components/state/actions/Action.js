// FETCH ACTION NAMES

export const ADD_NOTE = 'ADD_NOTE';
export const DEL_NOTE = 'DEL_NOTE';
export const EDIT_NOTE = 'EDIT_NOTE';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';


// ACTION GENERATORS

export const onAddNoteButton = (note, priority) => ({
    type: ADD_NOTE,
    payload: {note, priority}
});

export const onDeleteNoteButton = (id) => ({
    type: DEL_NOTE,
    payload: id
});

export const EditButton = (id, title) => ({
    type: EDIT_NOTE,
    payload: {id, title}
});

export const setVisibilityFilter = (filter) => ({
    type: SET_VISIBILITY_FILTER,
    filter
});