// IMPORT PACKAGE REFERENCES

import { combineReducers } from 'redux';


// IMPORT REDUCERS

import noteReducer from './Reducer';
import visibilityFilter from './Filter';


// EXPORT APP REDUCER

export const AppReducer = combineReducers({
    noteReducer,
    visibilityFilter
});