import {
    ADD_NOTE,
    DEL_NOTE,
    EDIT_NOTE
} from '../actions/Action';

const initialState = {
    items:[],
};

const noteReducer = (state = initialState, action) => {
    let stateCopy = state;
    switch (action.type) {
        case ADD_NOTE:
            return {
                ...state,
                items: [
                    ...state.items,
                    {
                        id: stateCopy.items.reduce((maxId, note) => Math.max(note.id, maxId), -1) + 1,
                        title: action.payload.note,
                        priority: action.payload.priority,
                        time: new Date().toLocaleTimeString() + ' ' + new Date().toLocaleDateString()
                    }
                ]
            };
        case DEL_NOTE:
            return {
                ...state,
                items: state.items.filter(item => item.id !== action.payload)
            };
        case EDIT_NOTE: {
            const { id, title } = action.payload;
            const editTitle = (id, title) => {
                let item = stateCopy.items.find(x => x.id === id); //Поиск и изменение значения в массиве объектов
                item.title = title;
            };
            editTitle(id, title);
            return state;
        }
        default:
            return state;
    }
};

export default noteReducer;