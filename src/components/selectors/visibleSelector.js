import { createSelector } from 'reselect';

const getFilter = (state) => state.visibilityFilter;
const getNotes = (state) => state.noteReducer.items;

export const getVisibleNotes = createSelector(
    [getFilter, getNotes],
    (filter, items) => {
        switch (filter) {
            case 'all':
                return items;
            case 'high':
                return items.filter(note => note.priority === 'high');
            case 'medium':
                return items.filter(note => note.priority === 'medium');
            case 'low':
                return items.filter(note => note.priority === 'low');
        }
    }
);